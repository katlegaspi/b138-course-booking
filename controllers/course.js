const Course = require("../model/Course");

// Create a new course
module.exports.addCourse = async (user, reqBody) => {
	if(user){
		let newCourse = new Course({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		})

		return newCourse.save().then((course, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	}
	else{
		return (`You are not authorized to do this action.`);
	}
}

// Controller method for retrieving all the courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}

// Retrieve all active courses
module.exports.getAllActive = () => {
	return Course.find({isActive : true}).then(result => {
		return result;
	})
}

// Retrieving a specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}

// Update a course
module.exports.updateCourse = async (user, reqParams, reqBody) => {
	// Validate if user is Admin
	if(user.isAdmin){
		// Specify the fields/properties of the document to be updated
		let updatedCourse = {
			name : reqBody.name,
			description : reqBody.description,
			price : reqBody.price
		}
		return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	}else{
		return (`You are not authorized to do this action.`);
	}
}

// START OF ACTIVITY - S35
// Controller for archiving a course
module.exports.archiveCourse = async (user, reqParams, reqBody) => {
	if(user.isAdmin){
		let archivedCourse = {
			isActive : reqBody.isActive
		}
		return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((course, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	}else{
		return (`You are not authorized to do this action.`);
	}
}
// END OF ACTIVITY - S35