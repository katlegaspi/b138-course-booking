const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth");

// Route for checking if the user's email already exists in the database
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body)
	.then(resultFromController => res.send(resultFromController));
});

// Route for registering a user
router.post("/register", (req, res) => {
	userController.registerUser(req.body)
	.then(resultFromController => res.send(`User registered. ${resultFromController}`));
});

// Route for authenticating a user
router.post("/login", (req, res) => {
	userController.loginUser(req.body)
	.then(resultFromController => res.send(resultFromController));
});

/*// S33 ACTIVITY: Create a /details route that will accept the user’s Id to retrieve the details of a user.
router.post("/details",(req,res) => {
	userController.getProfile(req.body)
	.then(resultFromController => res.send(resultFromController));
})*/

// S33 SOLUTION: Route for retrieving user details
// auth.verify method acts as a middleware to ensure that the user is logged in before they can access specific app features
router.get("/details", auth.verify, (req, res) => {
	
	const userData = auth.decode(req.headers.authorization);
	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId : userData.id})
	.then(resultFromController => res.send(resultFromController));
})

// Route to enroll user to a course
router.post("/enroll", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	let data = {
		userId: req.body.userId,
		courseId: req.body.courseId
	}
	userController.enroll(data, userData.isAdmin)
	.then(resultFromController => res.send(resultFromController));
});


// Allows us to export the "router" object that will be accessed in "index.js"
module.exports = router;